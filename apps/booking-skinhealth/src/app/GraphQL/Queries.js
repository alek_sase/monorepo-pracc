import { gql } from "@apollo/client";

export const LOAD_DATA = gql`
  {
    master_categories {
      id
      name
      image
      categories {
        id
        master_category_id
        name
        services {
          category_id
          id
          in_clinic
          name
          price
        }
      }
    }
  }
`;
