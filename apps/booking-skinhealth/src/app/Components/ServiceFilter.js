import {
  MedicineBoxOutlined,
  LaptopOutlined,
  ClockCircleOutlined,
  StarFilled,
} from "@ant-design/icons";

function ServiceFilter({ serviceFilter, setServiceFilter, data }) {
  return (
    <div>
      <div className="filters">
        <span
          onClick={function () {
            setServiceFilter("clinic");
          }}
          className={
            serviceFilter === "clinic" ? "inClinic activeSpan" : "inClinic"
          }
        >
          <MedicineBoxOutlined style={{ marginRight: "8px" }} /> In Clinic
        </span>
        <span
          onClick={function () {
            setServiceFilter("virtual");
          }}
          className={
            serviceFilter === "virtual" ? "inClinic activeSpan" : "inClinic"
          }
        >
          <LaptopOutlined style={{ marginRight: "8px" }} /> Virtual Consultation
        </span>
      </div>
      <div className="filtersBox">
        {serviceFilter === "clinic" &&
          data.map((x) => {
            if (x.in_clinic === true)
              return (
                <div className="info" key={x.id}>
                  <div className="titleMoney">
                    <h4>{x.name}</h4>
                    <span>£ {x.price}</span>
                  </div>
                  <span style={{ color: "gray" }}>
                    {x.time} min <ClockCircleOutlined />
                  </span>
                  <div className="starsDiv">
                    <ul>
                      <li>
                        <StarFilled
                          style={{ fontSize: "22px", color: "yellow" }}
                        />
                      </li>
                      <li>
                        <StarFilled
                          style={{ fontSize: "22px", color: "yellow" }}
                        />
                      </li>
                      <li>
                        <StarFilled
                          style={{ fontSize: "22px", color: "yellow" }}
                        />
                      </li>
                      <li>
                        <StarFilled
                          style={{ fontSize: "22px", color: "yellow" }}
                        />
                      </li>
                      <li>
                        <StarFilled
                          style={{ fontSize: "22px", color: "yellow" }}
                        />
                      </li>
                    </ul>
                    <span>{x.review} reviews</span>
                  </div>
                </div>
              );
          })}
        {serviceFilter === "virtual" &&
          data.map((x) => {
            if (x.in_clinic === false || x.in_clinic === undefined)
              return (
                <div className="info" key={x.id}>
                  <div className="titleMoney">
                    <h4>{x.name}</h4>
                    <span>£ {x.price}</span>
                  </div>
                  <span style={{ color: "gray" }}>
                    {x.time} min <ClockCircleOutlined />
                  </span>
                  <div className="starsDiv">
                    <ul>
                      <li>
                        <StarFilled
                          style={{ fontSize: "22px", color: "yellow" }}
                        />
                      </li>
                      <li>
                        <StarFilled
                          style={{ fontSize: "22px", color: "yellow" }}
                        />
                      </li>
                      <li>
                        <StarFilled
                          style={{ fontSize: "22px", color: "yellow" }}
                        />
                      </li>
                      <li>
                        <StarFilled
                          style={{ fontSize: "22px", color: "yellow" }}
                        />
                      </li>
                      <li>
                        <StarFilled
                          style={{ fontSize: "22px", color: "yellow" }}
                        />
                      </li>
                    </ul>
                    <span>{x.review} reviews</span>
                  </div>
                </div>
              );
          })}
      </div>
    </div>
  );
}

export default ServiceFilter;
