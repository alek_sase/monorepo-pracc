import React, { useState } from "react";

function OptionSelect({ data, handleFilteredOptions }) {
  const [selectedOption, setSelectedOption] = useState("Botox");
  
  return (
    <div className="optionSelect">
      {data.map((x) => (
        <div
          key={x.id}
          onClick={function () {
            handleFilteredOptions(x.name);
            setSelectedOption(x.name);
          }}
          className={
            selectedOption === x.name ? "optItem activeOptItem" : "optItem"
          }
        >
          <span>{x.name}</span>
          <span className="quantity">{x.rdmValue}</span>
        </div>
      ))}
    </div>
  );
}

export default OptionSelect;
