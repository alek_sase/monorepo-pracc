import React from "react";

import logo from "../img/logo.png";

function Navbar(props) {
  return (
    <div className="navbar">
      <img src={logo} alt="logo" />

      <div className="chooseService">
        <h4>Choose Service</h4>
        <span>Step 1/8</span>
      </div>
    </div>
  );
}

export default Navbar;
