import React, { useEffect, useState } from "react";
// import { defaultItems } from "../db/data";
import { AppstoreOutlined, CreditCardOutlined } from "@ant-design/icons";
import OptionSelect from "./OptionSelect";
import ServiceFilter from "./ServiceFilter";

//GraphQL
import { useQuery } from "@apollo/client";
import { LOAD_DATA } from "../GraphQL/Queries";

function HomeContent(props) {
  //DataLoad
  const { error, loading, data } = useQuery(LOAD_DATA);

  console.log(data);

  const [masterData, setMasterData] = useState([]);

  const [active, setActive] = useState("all");
  const [filterActive, setFilterActive] = useState([]);
  const [serviceFilter, setServiceFilter] = useState("clinic");
  const [filteredOptions, setFilteredOptions] = useState([]);

  useEffect(() => {
    if (data) {
      setMasterData(data.master_categories);
    }
  }, [data]);

  useEffect(() => {
    showAllCategories();
  }, []);

  function handleFilteredOptions(current) {
    const arr = [];
    filterActive.map((x) => {
      if (current === x.name) {
        x.services?.map((y) => {
          arr.push({
            id: y.id,
            name: y.name,
            price: y.price,
            in_clinic: y.in_clinic,
          });
        });
      }
    });
    setFilteredOptions(arr);
  }
  // console.log(filterActive);
  function showAllCategories() {
    const arr = [];
    masterData.map((x) => {
      x.categories.map((y) =>
        arr.push({
          id: y.id,
          name: y.name,
          services: y.services,
        })
      );
    });
    setFilterActive(arr);
  }

  return (
    <div className="homeContent">
      <div className="options">
        <div
          onClick={function () {
            setActive("all");
            showAllCategories();
          }}
          className={active === "all" ? "item activeItem" : "item"}
        >
          <AppstoreOutlined
            style={{ fontSize: "40px", color: "#54b2d3" }}
            theme="outlined"
          />
          <span>All</span>
        </div>
        {masterData.map((x) => (
          <div
            key={x.id}
            onClick={function () {
              setActive(x.name);
              setFilterActive(x.categories);
              handleFilteredOptions(x.categories[0].name);
            }}
            className={active === `${x.name}` ? "item activeItem" : "item"}
          >
            <img src={require(`../img/${x.image}.svg`).default} alt="icon" />
            <span>{x.name}</span>
          </div>
        ))}
        <div className="item">
          <CreditCardOutlined
            style={{ fontSize: "40px", color: "rgba(255, 166, 0, 0.501)" }}
            theme="outlined"
          />
          <span>Voucher</span>
        </div>
      </div>

      <div className="secondRow">
        <OptionSelect
          data={filterActive}
          handleFilteredOptions={handleFilteredOptions}
        />
        <div className="optionDetails">
          <ServiceFilter
            serviceFilter={serviceFilter}
            setServiceFilter={setServiceFilter}
            data={filteredOptions}
          />
        </div>
      </div>
    </div>
  );
}

export default HomeContent;
