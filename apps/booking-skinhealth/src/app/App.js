import "./App.css";
import Homepage from "./Components/Homepage";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
  from,
} from "@apollo/client";
import { onError } from "@apollo/client/link/error";

const errorLink = onError(({ graphqlErrors, networkError }) => {
  if (graphqlErrors) {
    graphqlErrors.map(({ message, location, path }) => {
      return console.log(`GraphQL error ${message} `);
    });
  }
});

const link = from([
  errorLink,
  new HttpLink({ uri: "http://localhost:8080/v1/graphql" }),
]);

const typePolicies = {
  master_categories: {
    keyFields: ["id"],
  },
};

const client = new ApolloClient({
  cache: new InMemoryCache({ typePolicies }),
  link: link,
});

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <Homepage />
      </div>
    </ApolloProvider>
  );
}

export default App;
