# pabau-ui

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test pabau-ui` to execute the unit tests via [Jest](https://jestjs.io).
