import { render } from '@testing-library/react';
import SelectOption from './select-option';
describe('SelectOption', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SelectOption />);
    expect(baseElement).toBeTruthy();
  });
});
