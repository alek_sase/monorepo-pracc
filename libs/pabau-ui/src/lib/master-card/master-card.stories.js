import { MasterCard } from '@monorepo-pabau/pabau-ui';
import someIcon from '../../../../../apps/booking-skinhealth/src/app/img/acne.svg';

export default {
  component: MasterCard,
  title: 'MasterCard',
  args: {
    x: {
      name: 'Aleksandar',
      icon: someIcon,
    },
  },
  argTypes: {},
};

const Template = (args) => <MasterCard {...args} />;

export const Primary = Template.bind({});
Primary.args = {};
