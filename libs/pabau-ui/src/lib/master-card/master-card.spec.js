import { render } from '@testing-library/react';
import MasterCard from './master-card';
describe('MasterCard', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<MasterCard />);
    expect(baseElement).toBeTruthy();
  });
});
