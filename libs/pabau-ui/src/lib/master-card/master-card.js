import styles from './master-card.module.css';
export function MasterCard({ x, onClick, active }) {
  return (
    <div
      key={x.key}
      onClick={() => onClick(x)}
      className={
        active === `${x.name}`
          ? `${styles.item} ${styles.activeItem} `
          : `${styles.item}`
      }
    >
      <img src={x.icon} alt="icon" />
      <span>{x.name}</span>
    </div>
  );
}
export default MasterCard;
